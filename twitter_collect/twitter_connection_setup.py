import tweepy
# We import our access keys:
from tweet_collection.credentials import *

def twitter_setup():
    """
    Utility function to setup the Twitter's API
    with an access keys provided in a file credentials.py
    :return: the authentified API
    """
    # Authentication and access using keys:
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_SECRET)

    # Return API with authentication:
    api = tweepy.API(auth)
    return api

def collect():
    '''
    This function utilises a Twitter connexion in order to search for tweets
    :return: The result of function search() of the API
    '''
    connexion = twitter_setup()
    tweets = []
    tweets = connexion.search("Emmanuel Macron", language="french", rpp=100)
    # for tweet in tweets:
    #     # print(tweet)
    #     tweets.append(tweet)
    return tweets

collect() # Show up to 100 tweets about "Emmanuel Macron"


def collect_by_user(user_id):
    '''
    This function utilises a Twitter connexion in order to get (up to 200) timelines tweets of a user of Twitter
    :param user_id:
    :return: The result of function user_timeline() of the API
    '''
    connexion = twitter_setup()
    statuses = connexion.user_timeline(id=user_id, count=200)
    for status in statuses:
        print(status.text)
    return statuses

# collect_by_user(1976143068) # Get up to  200 tweets (timeline) of Emmanuel Macron



from tweepy.streaming import StreamListener
class StdOutListener(StreamListener):

    def on_data(self, data):
        # print(data)
        return True

    def on_error(self, status):
        if  str(status) == "420":
            # print(status)
            print("You exceed a limited number of attempts to connect to the streaming API")
            return False
        else:
            return True




def collect_by_streaming(data):
    '''
    This function has as objective get actual tweets filtering by the track
    :return: None, but it continuously prints the tweets
    '''

    connexion = twitter_setup()
    listener = StdOutListener()
    stream=tweepy.Stream(auth = connexion.auth, listener=listener)
    stream.filter(track=data)



# collect_by_streaming(['Macron'])  # Tweets in real-time
