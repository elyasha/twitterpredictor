Twitter Predictor

L'objectif de ce mini-projet est de developper, de manière très incrémentale, un système d'analyse de tweets comme prédicteur de résultats d'élections.
Implémenter une version simple de l'outil d'analyse de tweets que l'on pourrait qualifier de MVP (Minimum Viable product).
Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.
Prerequisites
What things you need to install the software and how to install them
Give examples
Installing
A step by step series of examples that tell you how to get a development env running
Say what the step will be
Give the example
And repeat
until finished
End with an example of getting some data out of the system or using it for a little demo
Running the tests
Explain how to run the automated tests for this system
Break down into end to end tests
Explain what these tests test and why
Give an example
And coding style tests
Explain what these tests test and why
Give an example
Deployment
Add additional notes about how to deploy this on a live system
Built With
Dropwizard - The web framework used
Maven - Dependency Management
ROME - Used to generate RSS Feeds
Contributing
Please read CONTRIBUTING.md for details on our code of conduct, and the process for submitting pull requests to us.
Versioning
We use SemVer for versioning. For the versions available, see the tags on this repository.
Authors
Matheus Elyasha LOPES - CentraleSupélec
contributors
Imane Boha - CentraleSupélec
License
This project is licensed under the CentraleSupélec License - see the LICENSE.md file for details
Acknowledgments
Hat tip to anyone whose code was used
Inspiration
etc
https://gitlab-student.centralesupelec.fr/celine.hudelot/cs_coding_weeks_tweeter_analysis
MVP (Minimum Viable Product)
Permettra de se connecter à l'API Twitter pour collecter un ensemble de tweets relatifs à une élection.
Se basera uniquement sur l'analyse du contenu textuel des tweets et négligera l'information sur l'auteur du tweet, son type (retweet, reply,...) et son contenu multimédia.
Permettra un traitement et une analyse simples des tweets.
Affichera les résultats de l'analyse sous la forme d'un graphe temporel comme ci-dessus.