
from twitter_collect.twitter_connection_setup import *
b=collect()

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def tweet_to_dict (tweet):
    dict =({ "user" : tweet.id,"text":tweet.text ,"created_at" :str(tweet.created_at)})
    return dict

def transform_to_dataframe (tweets_data) :
    # fonction qui prend en argument une liste de tweets et qui la transforme en data
    utile_tweets= []
    for tweet in tweets_data:

        utile_tweet= {"tweet_textual_content": str(tweet.text),
                "id": tweet.id,
                "hashtags": tweet.entities["hashtags"],
                "retweeted": tweet.retweeted,
                "RTs": tweet.retweet_count,
                "likes": tweet.favorite_count,
                "created_at": tweet.created_at,
                "followers": tweet.user.followers_count,
                "user": tweet.user.name}
        utile_tweets += [utile_tweet]
    dtframe = pd.DataFrame(utile_tweets)
    print(dtframe)

    return dtframe

transform_to_dataframe(collect())





def test_collect():
    tweets = collect()
    data =  transform_to_dataframe(tweets)
    assert 'tweet_textual_content' in data.columns
test_collect()

