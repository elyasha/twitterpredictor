from twitter_predictor.twitter_collect import collect
from pytest import *


def test_collect():
    tweets = tweet_collect.collect()
    data =  transform_to_dataframe(tweets)
    assert 'tweet_textual_content' in data.columns

# from twitter_collect.collect_tweets_users import collect_by_user
# from twitter_collect.to_dataframe import transform_to_dataframe
#
# def test_collect(search_term = 'EmmanuelMacron'):
#     tweets = collect_by_user(search_term,20)
#
#     data = transform_to_dataframe(tweets)
#     print('\n')
#     print(data)
#
#     return data
#     #assert 'tweet_textual_content' in data.columns
