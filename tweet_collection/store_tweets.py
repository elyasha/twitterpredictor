
import tweepy
import pandas as pd # We are going to use pd.DataFrame()
from tweet_collection.collect_candidate_actuality_tweets import get_candidate_queries
import json # We are going use json.dump() pour faire the "sérialisation"
from twitter_collect.twitter_connection_setup import *

from tweet_collection.collect_candidate_actuality_tweets import get_tweets_from_candidates_search_queries
## That script is for the 1 part of fonctionality 4. Here we are looking to store the tweets at HD.

connexion = twitter_setup()

def serialize_tweets(tweet):
    """a = collect()
    Transforms a object tweet in a simple dictionary
    :param tweet: a tweet in the original Tweepy object
    :return: (dict) a dictionary with basics information of the tweet
    """
    if isinstance(tweet, tweepy.models.Status):
        return {"text": str(tweet.text),
                "id": tweet.id,
                "retweeted": tweet.retweeted,
                "retweet_count": tweet.retweet_count,
                "favorite_count": tweet.favorite_count,
                "created_at": tweet.created_at.isoformat(),
                "followers_count": tweet.user.followers_count,
                "description": str(tweet.user.description),
                "name": tweet.user.name}
    raise TypeError(repr(tweet) + " Not the same object type")


# print(serialize_tweets(collect()[0]))

def store_tweets(tweets, filename):
    """
    Makes a search of tweets with the subjects in the list of queries, and save them in filename, with JSON format
    :param queries: list of strings to search
    :param filename: filename of the file to store the data
    :return: nothing
    """
    file_path = "/home/elyasha/PycharmProjects/CodingWeeks/twitterPredictor/CandidateData/"
    json_to_dump = []

    for tweet in tweets:
        json_to_dump.append(serialize_tweets(tweet))

    with open(file_path + filename, 'w', encoding='utf-8') as f:
        json.dump(json_to_dump, f, indent=4)



# store_tweets(get_tweets_from_candidates_search_queries(["Emmanuel","Macron","EnMarche"],connexion), "tweets_candidate_1976143068")

if __name__ == "__main__":
    num_candidate = 1976143068
    queries = get_candidate_queries(num_candidate, "/home/elyasha/PycharmProjects/CodingWeeks/twitterPredictor/CandidateData/","hashtags")
    api_instance = twitter_setup()
    queries_results = get_tweets_from_candidates_search_queries(queries, api_instance)
    output_filename = '../CandidateData/tweets_candidate_'+ str(num_candidate) + '.json'
    store_tweets(queries_results, output_filename)

#
#
#
# def transform_to_dataframe(tweetdata):
#     '''
#
#     :param data: numpy ndarray (structured or homogeneous), dict, or DataFrame
#     :param index: Index or array-like
#     :param columns: Index or array-like
#     :param dtype: dtype, default None
#     :param copy:  boolean, default False
#     :return:
#     '''
#     return pd.DataFrame({"Message:" : tweetdata.txt,
#                         "Id:" : tweetdata.id,
#                         "Date & Heure:" : tweetdata.create_at})
#

def collect_to_pandas_dataframe():
   connexion = connect.twitter_setup()
   tweets = connexion.search("@EmmanuelMacron",language="fr",rpp=100)
   data = pd.DataFrame(data=[tweet.text for tweet in tweets], columns=['tweet_textual_content'])
   data['len']  = np.array([len(tweet.text) for tweet in tweets])
   data['ID']   = np.array([tweet.id for tweet in tweets])
   data['Date'] = np.array([tweet.created_at for tweet in tweets])
   data['Source'] = np.array([tweet.source for tweet in tweets])
   data['Likes']  = np.array([tweet.favorite_count for tweet in tweets])
   data['RTs']    = np.array([tweet.retweet_count for tweet in tweets])
   return data

