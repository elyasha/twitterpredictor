from typing import List, Any

from twitter_collect.twitter_connection_setup import twitter_setup,collect_by_streaming
from tweet_collection.collect_candidate_actuality_tweets import get_candidate_queries
from tweepy.streaming import StreamListener
from time import time

import tweepy

def get_replies_to_candidate(num_candidate):
    '''

    :param num_candidate: The number of the candidate
    :return:
    '''
    connexion = twitter_setup()
    replies = []
    tweets_ids = []
    #  recupere les messages recents du candidat  num_candidate
    statuses = connexion.user_timeline(id=num_candidate, language="french", rpp=100) # There are the tweets of the candidate num_candidate
    for status in statuses: # query pour retrouver des tweets repondant a l'utilisateur num_candidate
        # print(status)
        # query pour retrouver des tweets repondant a l'utilisateur user_id
        tweets_ids.append(status.id_str)
    #     print(tweet_id)
    # print(tweets_ids)

    if num_candidate == 1976143068:
        user_name = "@emmanuelmacron"
    elif num_candidate == 374392774:
        user_name = "@PhilippePoutou"
    else:
        user_name = input("Please enter the name of the user: (with @ and no spaces")

    for tweet in connexion.search(q=user_name, since_id=992433028155654144, result_type='recent', timeout=999999):
        # print(tweet)
     # si le tweet renvoye par la requete possede un champs "in reply_to__status_id_str" cest a dire si cest une reponse a un tweet
     # if hasattr(tweet, 'in_reply_to_status_id_str'):
    #     # si c'ets une reponse au tweet actuel (full_tweet) du candidat
     #     if (tweet.in_reply_to_status_id_str == full_tweet.id_str):
      #         replies.append(tweet.text)
     #         # print(tweet.text)
        if tweet.in_reply_to_status_id_str is not None:
            replies.append(tweet)

    # for reply in replies:
    #     print(reply.text)
    return replies

# print(get_replies_to_candidate(1976143068)) #
#
# def get_retweets_of_candidate(num_candidate):
#     connexion = twitter_setup()
#     retweets = {}
#     # recupere les retweets recents du candidat  num_candidat
#     # for tweet in connexion.search(since_id=num_candidate, result_type='recent', timeout=999999):
#     #     if tweet.text[:2] == 'RT':
#     #         retweets.append(tweet)
#     #
#     # for full_tweet in connexion.user_timeline(id=num_candidate, language="fr", rpp=100):
#     #     # print(full_tweet.text)
#     #     # query pour retrouver des tweets repondant a l'utilisateur num_candidat
#     #     query = "to:" + num_candidate
#     # # print(query)
#     #
#     # for tweet in connexion.search(q=query, since_id=992433028155654144, result_type='recent', timeout=999999):
#     #     # print(tweet.text)
#     #     # si le tweet renvoye par la requete possede un champs "in reply_to__status_id_str" cest a dire si cest une reponse a un tweet
#     #     if hasattr(tweet, 'in_reply_to_status_id_str'):
#     #         # si c'est une reponse au tweet actuel (full_tweet) du candidat
#     #         if (tweet.in_reply_to_status_id_str == full_tweet.id_str):
#     #             replies.append(tweet.text)
#     #             print(tweet.text)
#     query = ':to' + num_candidate
#     for tweet in connexion.search(q=query, since_id=num_candidate, result_type='recent', timeout=9999):
#         retweets[tweet] = tweet.retweet(tweet.id)
#     return retweets
#
# get_retweets_of_candidate(992433028155654144)
#

def get_retweets_of_candidate(num_candidate):
    '''

    :param num_candidate:
    :return:
    '''

    connexion = twitter_setup()
    retweets = []
    tweets_ids = []
    #  recupere les messages recents du candidat  num_candidate
    statuses = connexion.user_timeline(id=num_candidate, language="french",
                                       rpp=100)  # There are the tweets of the candidate num_candidate
    for status in statuses:  # query pour retrouver des tweets repondant a l'utilisateur num_candidate
        # print(status)
        # query pour retrouver des tweets repondant a l'utilisateur user_id
        tweets_ids.append(status.id_str)
    #     print(tweet_id)
    # print(tweets_ids)
    for tweet_id in tweets_ids:
        retweets_id = connexion.retweets(tweet_id,100)
        for retweet in retweets_id:
            # print(retweet)
            # print(retweet.text)
            retweets.append(retweet)

    return retweets

# get_retweets_of_candidate(1976143068)

def get_flux_of_tweets_of_candidate(num_candidate,file_type="keywords"):
    '''

    :param num_candidate:
    :return:
    '''

    replies: List[Any] = []
    tweets = []
    file_path = "/home/elyasha/PycharmProjects/CodingWeeks/twitterPredictor/CandidateData/"
    #
    #
    # connexion = twitter_setup()
    # listener = StdOutListener()
    #
    # queries = get_candidate_queries(num_candidate, file_path, file_type)
    # stream = tweepy.Stream(auth=connexion.auth, listener=listener)
    # stream.filter(track=queries)
    #
    # for x in stream:
    #     print(x.text)
    class StdOutListener(StreamListener):

        def __init__(self,start_time,time_limit=10):
            self.time = start_time
            self.limit = time_limit

        def on_data(self, data):
            while (time() - self.time) < self.limit:
                # print(data)
                tweets.append(data)
            return True

        def on_error(self, status):
            if str(status) == "420":
                # print(status)
                print("You exceed a limited number of attempts to connect to the streaming API")
                return False
            elif time() - self.time >= self.time:
                print("time is over!")
                return False
            else:
                return True



    connexion = twitter_setup()
    start_time = time()
    stop_time = 10
    listener = StdOutListener(start_time,stop_time)
    stream = tweepy.Stream(auth=connexion.auth, listener=listener)
    stream.filter(track=get_candidate_queries(num_candidate,file_path,file_type))

    stream.disconnect()
    #
    #
    # for tweet in tweets:
    #     if tweet.in_reply_to_user_id == num_candidate:
    #         replies.append(tweet)
    #         print(tweet.text)
    #
    # for reply in replies:
    #     print(reply.text)


# get_flux_of_tweets_of_candidate(1976143068)


def get_flux_of_replies_of_candidate(num_candidate,file_type="keywords"):
    '''

    :param num_candidate:
    :return:
    '''

    replies = []
    file_path = "/home/elyasha/PycharmProjects/CodingWeeks/twitterPredictor/CandidateData/"
    #
    #
    # connexion = twitter_setup()
    # listener = StdOutListener()
    #
    # queries = get_candidate_queries(num_candidate, file_path, file_type)
    # stream = tweepy.Stream(auth=connexion.auth, listener=listener)
    # stream.filter(track=queries)
    #
    # for x in stream:
    #     print(x.text)
    class StdOutListener(StreamListener):

        def __init__(self,start_time,time_limit=60):
            self.time = start_time
            self.limit = time_limit

        def on_data(self, data):
            while (time() - self.time) < self.limit:
                 if data['in_reply_to_user_id'] == num_candidate:
                     print(data)
                     replies.append(data)
            return True

        def on_error(self, status):
            if str(status) == "420":
                print(status)
                print("You exceed a limited number of attempts to connect to the streaming API")
                return False
            elif time() - self.time >= self.time:
                print("time is over!")
                return False
            else:
                return True



    connexion = twitter_setup()
    start_time = time()
    stop_time = 60
    listener = StdOutListener(start_time,stop_time)
    stream = tweepy.Stream(auth=connexion.auth, listener=listener)
    # stream.filter(track=get_candidate_queries(num_candidate,file_path,file_type))
    stream.filter(track=get_candidate_queries(num_candidate,file_path,file_type))

    stream.disconnect()




# get_flux_of_replies_of_candidate(1976143068)