from twitter_collect import twitter_connection_setup
from tweepy.streaming import StreamListener
import tweepy

def get_candidate_queries(num_candidate, file_path,file_type):
    """
    Generate and return a list of string queries for the search Twitter API from the file file_path_num_candidate.txt
    :param num_candidate: the number of the candidate
    :param file_path: the path to the keyword and hashtag
    files
    :param type: type of the keyword, either "keywords" or "hashtags"
    :return: (list) a list of string queries that can be done to the search API independently
    """
    queries = []

    real_file_path = "{}{}_candidate_{}.txt".format(file_path,file_type,num_candidate)
    try:
        with open(real_file_path, 'r') as real_file:
            keywords = real_file.read().split("\n")

        i = 0
        for each_keyword in keywords:
            if file_type == "hashtag":
                queries.append("#{}".format(each_keyword))
            else:
                queries.append("{}".format(each_keyword))
            # if i < len(keywords)-2:
            #     for keyword2 in keywords[i+1:]:
            #         if file_type == "hashtag":
            #             queries.append("#{} AND #{}".format(keyword2, keyword2))
            #         else:
            #             queries.append("{} AND {}".format(keyword2, keyword2))
            i += 1

        return queries

    except IOError:
        print("file {} is missing.".format(real_file_path))
        return []

# print(get_candidate_queries(1976143068, "/home/elyasha/PycharmProjects/CodingWeeks/twitterPredictor/CandidateData/", "keywords"))
# This command makes the slip of the  keywords or hashtags




def get_tweets_from_candidates_search_queries(queries, twitter_api):

    all_tweets = []

    try:
        for query in queries:
            tweets = twitter_api.search(q=query, language="french", rpp="200")
            for tweet in tweets:
                # print(tweet.text)
                all_tweets.append(tweet)

    except tweepy.TweepError as e:

        if e.response.text in ["429","420"]:
            print("we exceeded the twitter rate limit, returning now")
            return all_tweets
        elif e.response.text =="500":
            print("Twitter down, Twitter down!")
            return None
        elif  e.response.text =="401":
            print("wrong credentials!")
            return None
        elif  e.response.text =="404":
            print("The request <{}> is invalid".format(query))
            return None
        else:
            "twitter API responded with code {}, something is wrong ".format(e.response.text)
            return None

    return all_tweets

# get_tweets_from_candidates_search_queries(['Bolsonaro','Jair'], twitter_connection_setup.twitter_setup())
# get_tweets_from_candidates_search_queries(['Bolsonaro'],twitter_connection_setup.twitter_setup())