import pytest
from twitter_collect.twitter_connection_setup import twitter_setup

def test_twitter_setup():
    assert twitter_setup() is not None
