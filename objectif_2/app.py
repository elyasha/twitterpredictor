import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
from tweet_analysis.tweet_anal import *

a=analyse_de_sentiments(transform_to_dataframe(collect()))[0]
b=analyse_de_sentiments(transform_to_dataframe(collect()))[1]
c=analyse_de_sentiments(transform_to_dataframe(collect()))[2]
d=analyse_de_sentiments(transform_to_dataframe(collect(query="Donald Trump")))[0]
e=analyse_de_sentiments(transform_to_dataframe(collect(query="Donald Trump")))[1]
f=analyse_de_sentiments(transform_to_dataframe(collect(query="Donald Trump")))[2]
g=analyse_de_sentiments(transform_to_dataframe(collect(query="Jair Bolsonaro")))[0]
h=analyse_de_sentiments(transform_to_dataframe(collect(query="Jair Bolsonaro")))[1]
i=analyse_de_sentiments(transform_to_dataframe(collect(query="Jair Bolsonaro")))[2]

import dash
import dash_core_components as dcc
import dash_html_components as html

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div(children=[
    html.H1(children='Hello CS , Coding Weeks are not going very well !'),

    html.Div(children='''
        Dash: A web application to predict election results.
    '''),

    dcc.Graph(
        id='example-graph',
        figure={
            'data': [
                {'x': [1, 2, 3], 'y': [a, b, c], 'type': 'bar', 'name': 'Emmanuel Macron'},
                {'x': [1, 2, 3], 'y': [d, e, f], 'type': 'bar', 'name': 'Donald Trump'},
                {'x': [1, 2, 3], 'y': [g, h, i], 'type': 'bar', 'name': 'Jair Bolsonaro'},

            ],
            'layout': {
                'title': 'Predicting election results'
            }
        }
    )
])

if __name__ == '__main__':
    app.run_server(debug=True)
print(d,e,f)

