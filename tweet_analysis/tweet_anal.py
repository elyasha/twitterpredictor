import numpy as np
from matplotlib.pyplot import *
from twitter_collect.twitter_connection_setup import *
from tweet_collection.test_transform_to_data_file import *
from textblob import TextBlob

def maximum_de_retweets (data) :
    rt_max  = np.max(data['RTs'])
    rt  = data[data.RTs == rt_max].index[0]
    # Max RTs:
    print("The tweet with more retweets is: \n{}".format(data['tweet_textual_content'][rt]))
    print("Number of retweets: {}".format(rt_max))
    print("{} characters.\n".format(data['len'][rt]))


def maximum_de_likes (data ):
    like_max = np.max(data['likes'])
    likes=data[data.likes == like_max].index[0]
    # Max like_data
    print ("The tweet with more likes is : \n{}".format(data['tweet_textual_content'][likes]))
    print ( "Number of retweets :{}.".format(like_max))
    # print ("{} characters. \n".format(data['len'][likes]))


def minimum_de_likes (data):
    like_min =np.min(data['likes'])
    likesmin=data[data.likes==like_min].index[0]
    # Min like_data
    print("The tweet with least likes is : \n{}".format(data['tweet_textual_content'][likesmin]))
    print ("Number of retweets :{}".format(like_min))
    # print ("{} characters. \n".format(data['len'][likesmin]))


def evol(data):
    tfav = pd.Series(data=data['likes'].values, index=data['created_at'])
    tret = pd.Series(data=data['RTs'].values, index=data['created_at'])

       # Likes vs retweets visualization:
    tfav.plot(figsize=(16,4), label="likes", legend=True)
    tret.plot(figsize=(16,4), label="Retweets", legend=True)
    plt.show()
evol(transform_to_dataframe(collect()))

def evol2(data) :
    tfollow=pd.Series (data=data['followers'].values, index=data['created_at'])
    tret=pd.Series(data=data['RTs'].values, index=data['created_at'])

    tfollow.plot(figsize=(16,4), label="followers", legend=True)
    tret.plot(figsize=(16,4), label="Retweets", legend=True)

    plt.show()
evol2(transform_to_dataframe(collect()))


def analyse_de_sentiments(data):
    textualcontent=data['tweet_textual_content']
    neutraltweets=0
    positiftweets=0
    negatiftweets=0
    for t in textualcontent:
        t=TextBlob(t)
        if t.sentiment.polarity < -0.1:
            negatiftweets=negatiftweets+1
        if t.sentiment.polarity > 0.1 :
            positiftweets=positiftweets +1
        else :
            neutraltweets=neutraltweets+1
    M=[positiftweets,negatiftweets,neutraltweets]
    return M


analyse_de_sentiments(transform_to_dataframe(collect()))










