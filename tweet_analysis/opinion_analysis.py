from twitter_collect.twitter_connection_setup import collect_by_user
from textblob import TextBlob
from textblob import Word


def extrat_words():

    tweets_text = []
    text_blob = ''
    text_temp = ''
    word_lemmatize = []

    #Take the text of tweets list
    list_tweets_status = collect_by_user("EmmanuelMacron",10)

    for tweets_status in list_tweets_status:
        tweets_text.append(tweets_status.text)

    #Transform the all text into TextBlob
    for text in tweets_text:
        text_temp += text
    text_blob = TextBlob(text_temp)

    #Lemmatize
    for word in text_blob.words:
        w = Word(word)
        word_lemmatize.append(w.lemmatize())

    #Transform into unique words
    words_lemmatize_set = set(word_lemmatize)

    return words_lemmatize_set #Return a list of words lemmatized

def sentiment_analysis(twitter_candidate_id):
    tweets_text = []
    tweets_positives = []
    tweets_negatives  = []
    tweets_neutral = []

    #Take the text of tweets list
    list_tweets_status = collect_by_user(twitter_candidate_id)

    for tweets_status in list_tweets_status:
        tweets_text.append(tweets_status.text)

    for text in tweets_text:
        analyse = TextBlob(text).sentiment.polarity

        if analyse > 0:
            tweets_positives.append(text)
        elif analyse < 0:
            tweets_negatives.append(text)
        else:
            tweets_neutral.append(text)

    return (len(tweets_positives), len(tweets_neutral), len(tweets_negatives))

def print_sentiment_analysis(results_analysis):

    total = results_analysis[0] + results_analysis[1] + results_analysis[2]

    print("Percentage of positive tweets: {}%".format((results_analysis[0]/total)*100))
    print("Percentage of neutral tweets: {}%".format((results_analysis[1]/total)*100))
    print("Percentage de negative tweets: {}%".format((results_analysis[2]/total)*100))


if __name__ == "__main__":
    print_sentiment_analysis(sentiment_analysis(1976143068))
