import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt

from tweet_analysis.tweet_opinion import analyze_opinion
from twitter_collect.collect_candidate_actuality_tweets import get_candidate_queries, \
    get_tweets_from_candidates_search_queries
from twitter_collect.to_dataframe import transform_to_dataframe
from twitter_collect.twitter_connection_setup import twitter_setup


def plot_opinion(data_dict):
    sns.set(style="whitegrid")

    # Set up the matplotlib figure
    f, ax = plt.subplots()

    # Generate some sequential data
    x = np.array(list(data_dict.keys()))
    y1 = np.array(list(data_dict.values()))

    sns.barplot(x=x, y=y1, ax=ax)

    ax.axhline(0, color="k", clip_on=False)
    ax.set_ylabel("Percentage")

    # Finalize the plot
    sns.despine(bottom=True)
    plt.setp(f.axes, yticks=[])
    plt.tight_layout(h_pad=2)

    plt.show()


if __name__ == "__main__":
    num_candidate = 4
    queries = get_candidate_queries(num_candidate, "../CandidateData")
    api = twitter_setup()
    results = get_tweets_from_candidates_search_queries(queries, api)
    data_frame = transform_to_dataframe(results)
    pos_tweets, neu_tweets, neg_tweets = analyze_opinion(data_frame)

    pos_per = (len(pos_tweets)*100)/len(data_frame['tweet_textual_content'])
    neu_per = (len(neu_tweets)*100)/len(data_frame['tweet_textual_content'])
    neg_per = (len(neg_tweets)*100)/len(data_frame['tweet_textual_content'])

    dict = {"Positive": pos_per, "Neutral": neu_per, "Negative": neg_per}

    print(pos_per, neu_per, neg_per)

    plot_opinion(dict)
