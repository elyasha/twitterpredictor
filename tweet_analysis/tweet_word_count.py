from textblob import TextBlob
import operator
from twitter_collect.collect_candidate_tweet_activity import candidate_tweets
from twitter_collect.to_dataframe import transform_to_dataframe
import re


def remove_links(text):
    '''
    Remove links url from the text, and remove ’ too
    :param text: (string) the text to be cleaned
    :return: (string) the text without urls and ’
    '''
    text = re.sub('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', '', text)
    text = text.replace("’", '')

    return text.replace('RT', '')


def count_words_tweets(data):
    '''
    Count each word in the texts of the tweets, and creates a dictionary with their frequency
    :param data: the dataframe with the tweets
    :return: (dict) the keys are the words, and the values are their frequency
    '''
    all_text = ""

    for text in (data["tweet_textual_content"]):
        all_text += ( text + " " )

    all_text = remove_links(all_text)
    all_text_textblob = TextBlob(all_text)

    text_clean = set(all_text_textblob.noun_phrases)
    word_counter = {}

    for word in text_clean:
        word_counter[word] = all_text_textblob.noun_phrases.count(word)

    return word_counter


def find_max_word(data):
    '''
    From a dataframe, identifies the word most shown
    :param data: dataframe with the tweets
    :return: (tuple) with the word and their frequency
    '''
    word_counter = count_words_tweets(data)

    return max( word_counter.items(), key=operator.itemgetter(1) )


if __name__ == "__main__":
    tweets = candidate_tweets("EmmanuelMacron")
    data_frame = transform_to_dataframe(tweets)
    print(find_max_word(data_frame))
