from twitter_collect.test_transform_to_dataframe import  test_collect
import numpy as np
import pandas as pd

def max_retweets(search_term):

    data = test_collect(search_term)
    rt_max  = np.max(data['RTs'])
    rt  = data[data.RTs == rt_max].index[0] #column number with the max retweet

    # Max RTs:
    print("The tweet with more retweets is: \n{}".format(data['tweet_textual_content'][rt]))
    print("Number of retweets: {}".format(rt_max))
    print("{} characters.\n".format(data['len'][rt]))
