import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from twitter_collect.test_transform_to_dataframe import test_collect
from twitter_collect.Opinion_Analysis import sentiment_analysis

def plot_like_x_retweet():
    data = test_collect()
    tfav = pd.Series(data['Likes'].values, data['Date'])
    tret = pd.Series(data['RTs'].values, data['Date'])

    tfav.plot(figsize=(16,4), label="Likes", legend=True)
    tret.plot(figsize=(16,4), label="Retweets", legend=True)


    plt.show()

def plot_sentiment_analysis():
    results_analysis = sentiment_analysis("EmmanuelMacron",50)

    total = results_analysis[0] + results_analysis[1] + results_analysis[2]

    sns.set(style="white", context="talk")

    # Set up the matplotlib figure
    f, ax  = plt.subplots(1, 1, figsize=(7, 5), sharex=True)

    # Generate some sequential data
    x = np.array(['Positive', 'Neutral', 'Negative'])
    y = [(results_analysis[0]/total)*100,(results_analysis[1]/total)*100,(results_analysis[2]/total)*100]

    sns.barplot(x=x, y=y, palette="rocket", ax=ax)
    ax.axhline(0, color="k", clip_on=False)
    ax.set_ylabel("Percentage %")
    plt.show()

plot_sentiment_analysis()
