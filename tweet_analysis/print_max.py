import numpy as np
from twitter_collect.collect_candidate_tweet_activity import candidate_tweets
from twitter_collect.to_dataframe import transform_to_dataframe


def tweet_max(data):
    """
    Finds the tweet indice in the dataframe that has more RTs
    :param data: dataframe of tweets to analyze
    :return: returns the indice in the dataframe in which contains the most retweeted tweet
    """
    return data['RTs'].idxmax()


def max_rt_candidate(candidate_username):
    """
    Prints the tweet with most RTs of a candidate
    :param candidate_username: the username of the candidate to analyze
    :return: nothing
    """
    results = candidate_tweets(candidate_username)
    data = transform_to_dataframe(results)
    rt = tweet_max(data)

    print("The tweet with more retweets is:\n{}".format(data['tweet_textual_content'][rt]))
    print("Number of retweets: {}".format(data['RTs'][rt]))
    print("{} characters.\n".format(data['len'][rt]))


if __name__ == "__main__":
    max_rt_candidate("EmmanuelMacron")
