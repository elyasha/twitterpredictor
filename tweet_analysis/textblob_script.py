from textblob import TextBlob
from tweet_collection.collect_candidate_tweet_activity import *
from twitter_collect.twitter_connection_setup import twitter_setup

connexion = twitter_setup()

def extract_vocabulary(tweets,stop_words = None):
    '''
    This function permits to extract the vocabulary of a set of tweets, recovering the unique words.
    :param tweets:
    :return:
    '''
    dict = {}
    tweets_text = ''
    for tweet in tweets:
        tweets_text += tweet.text

    tweets_text_blob = TextBlob(tweets_text)
    tweets_words = tweets_text_blob.words
    tweets_words_list = []

    for word in tweets_words:
        if not (word in stop_words):
            tweets_words_list.append(word)

    for word in tweets_words_list:
        dict[word] = tweets_words.count(word)

    return dict


extract_vocabulary(get_replies_to_candidate(1976143068), ['putain'])


